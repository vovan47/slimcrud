<?php

return [
    'settings' => [
        'db' => [
            // Illuminate/database configuration
            'driver'    => 'mysql',
            'host'      => 'localhost',
            'database'  => 'slimcrud',
            'username'  => 'root',
            'password'  => '',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ],
        'view' => [
            'template_path' => 'app/templates',
            'twig' => [
                'cache' => 'cache/twig',
                'debug' => true,
            ],
        ],
    ]
];
