<?php
// Route configuration

$app->get('/', 'Crud\AuthorController:listAuthors')->setName('list-authors');
$app->map(['GET', 'POST'], '/authors/{author_id:[0-9]+}/edit', 'Crud\AuthorController:editAuthor')->setName('edit-author');
$app->get('/authors/{author_id:[0-9]+}', 'Crud\AuthorController:listParams')->setName('author');