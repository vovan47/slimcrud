<?php
namespace Crud;

use Illuminate\Database\Eloquent\Model;
use Valitron\Validator;

final class Author extends Model
{
    /**
     * Turn off the created_at & updated_at columns
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Fields that can be updated via update()
     * @var array
     */
    protected $fillable = ['name', 'surname', 'patronymic', 'desc', 'pagetitle', 'subtitle'];

    /**
     * Update author with new data
     *
     * @param  arrray $data
     * @return null
     */
    public function update(array $data = [])
    {
        $validator = $this->getValidator($data);
        if (!$validator->validate()) {
            $messages = [];
            foreach ($validator->errors() as $fieldName => $errors) {
                $messages[] = current($errors);
            }
            $message = implode("\n", $messages);
            throw new \Exception($message);
        }

        return parent::update($data);
    }

    /**
     * Retrieve validator for this entity
     *
     * @param  Array $data Data to be validated
     * @return Validator
     */
    public function getValidator($data)
    {
        $validator = new Validator($data);

        $validator->rule('required', 'name');
        $validator->rule('lengthBetween', 'name', 1, 100);

        $validator->rule('required', 'surname');
        $validator->rule('lengthBetween', 'surname', 1, 100);

        $validator->labels([
            'name' => 'Name',
            'surname' => 'Surname',
        ]);

        return $validator;
    }
}
