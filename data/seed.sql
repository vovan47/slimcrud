/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.5.35-log : Database - slimcrud
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `authors` */

CREATE TABLE `authors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `desc` mediumtext,
  `surname` varchar(100) NOT NULL,
  `patronymic` varchar(100) NOT NULL,
  `pagetitle` varchar(100) DEFAULT NULL,
  `subtitle` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `authors` */

insert  into `authors`(`id`,`name`,`desc`,`surname`,`patronymic`,`pagetitle`,`subtitle`) values (1,'Suzanne','Suzanne Marie Collins is an American television writer and novelist, best known as the author of The Underland Chronicles and The Hunger Games trilogy','Collins','Marie','Another title','Test2'),(2,'Anne McCaffrey',NULL,'','',NULL,NULL),(3,'Peter ',NULL,'Hamilton','F. ',NULL,NULL),(4,'J. ',NULL,'Rowling','K. ',NULL,NULL),(5,'Ursula ','Test','Le Guin','',NULL,NULL),(6,'Terry ',NULL,'Pratchett','',NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
